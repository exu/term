fs = require 'fs'
debug = process.env.DEBUG
middlewareAdded = false
logger = null

module.exports = (exuServer, codeName, req, res, next)->
  logger = exuServer.buildLogger __filename

  myWebRoot = exuServer.exuWebRoot() + 'app/' + codeName
  reqFile = req.path.substr codeName.length + 1

  unless middlewareAdded
    exuServer.middleware.use myWebRoot, require('term.js').middleware()
    middlewareAdded = true

  if reqFile is '' # load index
    loadIndex exuServer, myWebRoot, res
  else
    logger.info "Load Exu Term's file #{reqFile}.", req.path
    if reqFile is '/term.js'
      do next
    else
      #res.sendFile __dirname + reqFile # needs Express ^4.8
      fs.readFile __dirname + reqFile, (err, data)->
        if err
          # TODO: find again with adding .ejs
          res.status(404).send err
        else
          # TODO: put file extension on type()
          res.type('css').send data

loadIndex = (exuServer, myWebRoot, res)->
  logger.info "Exu Term's main is running..." if debug

  termRoomName = '/term-' + Math.random().toString()[2..]
  termRoom = exuServer.io.of termRoomName

  term = launchTermServer exuServer, termRoom

  locals = {
    exu: exuServer
    env: process.env
    termRoomName: termRoomName
    myWebRoot: myWebRoot
    fsRoot: exuServer.exuWebRoot() + 'fs'
  }
  exuServer.middleware.render "#{__dirname}/index.html.ejs", locals, (err, html)->
    if err
      logger.error 'Fail to render...', err
    else
      res.send html

launchTermServer = (exuServer, termRoom)->
  pty = require 'pty.js'
  buff = []
  socket = null
  term = pty.fork process.env.SHELL || 'sh', [], {
    name: 'xterm'
    cols: 80
    rows: 24
    cwd: process.env.HOME
  }
  term.on 'data', (data)->
    if socket
      socket.emit 'data', {termData: data}
    else
      buff.push data
  termRoom.on 'connection', (theSocket)->
    socket = theSocket
    logger.info 'New term connection', socket.id
    socket.emit 'data', {termData: buff.shift()} while buff.length
    socket.on 'data', (data)->
      term.write data.termData if data.termData?
    socket.on 'resize', (size)->
      term.resize size.cols, size.rows
    socket.on 'disconnect', -> do term.destroy

